<?php

require_once ("animal.php");
require_once ("ape.php");
require_once ("frog.php");

$sheep = new Animal("shaun");

echo "Name : " . $sheep->getName() . "<br>"; // "shaun"
echo "Legs : " . $sheep->getLegs() . "<br>"; // 4
echo "Cold Blooded : " . $sheep->getColdBlooded() . "<br>"; // "no"

echo "<br>";

$kodok = new Frog("buduk");
echo "Name : " . $kodok->getName() . "<br>";
echo "Legs : " . $kodok->getLegs() . "<br>";
echo "Cold Blooded : " . $kodok->getColdBlooded() . "<br>";
echo "Jump : " . $kodok->jump() . "<br>"; // "hop hop"

echo "<br>";

$sungokong = new Ape("kera sakti");
echo "Name : " . $sungokong->getName() . "<br>";
echo "Legs : " . $sungokong->getLegs() . "<br>";
echo "Cold Blooded : " . $sungokong->getColdBlooded() . "<br>";
echo "Yell : " . $sungokong->yell() . "<br>"; // "Auooo"